package com.assignment1.com.assignment1.Models;

import java.io.Serializable;

public class DistanceMatrixModel implements Serializable {

    private int distance;
    private Double duration;
    private int idTrackable;
    private int idTracking;

    public void setIdTracking(int idTracking) {
        this.idTracking = idTracking;
    }

    public int getIdTracking() {
        return idTracking;
    }

    public int getDistance() {
        return distance;
    }

    public Double getDuration() {
        return duration;
    }

    public int getIdTrackable() {
        return idTrackable;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public void setIdTrackable(int idTrackable) {
        this.idTrackable = idTrackable;
    }
}
