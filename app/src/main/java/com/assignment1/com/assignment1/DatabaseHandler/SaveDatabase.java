package com.assignment1.com.assignment1.DatabaseHandler;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import com.assignment1.com.assignment1.Models.UserTrackingModel;
import com.assignment1.com.assignment1.Services.UserTrackingService;

import java.util.ArrayList;

public class SaveDatabase extends AsyncTask<Void,Integer,Void> {
    private final String LOG_TAG = this.getClass().getName();
    private UserTrackingService service;
    private ArrayList<UserTrackingModel>list = new ArrayList<>();
    private Context context;

    //this class is for storing user tracking's data to the database upon the stopping of an activity
    public SaveDatabase(Context context){
        this.context = context;
        this.service = UserTrackingService.getSingletonInstance(context);
    }

    @Override
    protected Void doInBackground(Void...unused){
        Log.i(LOG_TAG,"saving data to database");
        DatabaseHelper dbHelper = DatabaseHelper.getSingletonInstance(this.context);
        SQLiteDatabase write = dbHelper.getWriteDb();

        //get the list of user trackings from memory model
        this.list=service.getUserTrackingList();
        Log.i(LOG_TAG,"list size: "+this.list.size()+"");
        dbHelper.clearTable("UserTrackings");
        //put the contents into the user tracking list table
        for(int i=0; i<list.size(); i++){
            Log.i(LOG_TAG,list.get(i).getTitle());
            dbHelper.addUserTracking(list.get(i),write);
        }

        //dbHelper.closeDatabase();
        publishProgress(1);
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress)
    {
        if(progress.equals(1)){
            Log.i(LOG_TAG,"database updated with new data");
        }
        else if(progress.equals(0)){
            Log.i(LOG_TAG,"database not updated");
        }
    }

}
