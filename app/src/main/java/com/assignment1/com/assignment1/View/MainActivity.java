package com.assignment1.com.assignment1.View;

import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.os.AsyncTask;

import com.assignment1.com.assignment1.Controller.DistanceMatrixController;
import com.assignment1.com.assignment1.Controller.MainActivityListener;
import com.assignment1.com.assignment1.Controller.RecyclerViewDecorationController;
import com.assignment1.com.assignment1.Adapter.MainAdapter;
import com.assignment1.com.assignment1.DatabaseHandler.LoadDatabase;
import com.assignment1.com.assignment1.DatabaseHandler.SaveDatabase;
import com.assignment1.com.assignment1.R;


public class MainActivity extends AppCompatActivity{
    private RecyclerView myRecyclerView;
    private MainAdapter mainAdapter = new MainAdapter(this);
    private RecyclerView.LayoutManager myLayoutManager;
    private LoadDatabase loadDb;
    private SaveDatabase saveDb;
    private static final String LOG_TAG=MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //PreferenceManager.setDefaultValues(this,R.xml.preferences,false);
        //TextView text=findViewById(R.id.textView1);

        //load the file data into our database in an async task
        Log.i(LOG_TAG,"creating an async task");
        loadDb = new LoadDatabase(this);
        loadDb.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //set Recycler View
        setRecyclerView();

        //Set Action bar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Spinner setup
        setSpinner();

        //set fab
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.viewBookingFab);
        fab.setOnClickListener(new MainActivityListener.mainController(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.context_menu, menu);
        return true;
    }



    @Override
    protected void onStop(){
        super.onStop();
        saveDb = new SaveDatabase(this);
        saveDb.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setRecyclerView(){
        myRecyclerView = (RecyclerView) findViewById(R.id.bookingRecyclerView);
        myRecyclerView.setHasFixedSize(true);

        //set Layout for Recycler View
        myLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(myLayoutManager);

        //Set Recycler View Adapter
        myRecyclerView.setAdapter(mainAdapter);

        //set Recycler View Margin
        myRecyclerView.addItemDecoration(RecyclerViewDecorationController.createDecoration(this));

    }

    public void setSpinner(){
        final Spinner spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item_layout, getResources().getStringArray(R.array.categories));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String cat;
                String item = spinner.getSelectedItem().toString(); //I think this returning index id instead of cat name
                switch(item){
                    case "All":
                        mainAdapter.getAll();
                        myRecyclerView.setAdapter(mainAdapter);
                        break;

                    case "African":
                        cat = "African";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                    case "Argentinian":
                        cat = "Argentinian";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                    case "Asian":
                        cat = "Asian";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                    case "Dessert":
                        cat = "Dessert";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                    case "Italian":
                        cat = "Italian";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                    case "Thai":
                        cat = "Thai";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                    case "Vietnamese":
                        cat = "Vietnamese";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                    case "Western":
                        cat = "Western";
                        mainAdapter.filter(cat);
                        myRecyclerView.setAdapter(mainAdapter);
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });
    }

}

