package com.assignment1.com.assignment1.Models;
import java.io.Serializable;
import java.util.ArrayList;

/*
    Not sure whether i should implement Serializable or Parcelable
    Parcelable makes data processing much faster and efficient over Serializable since it does not create temporary objects
    However Serializable is much easier to implement and can be persisted
    Parcel is not recommended to be used in a persistent storage
*/

public class TrackableModel implements Serializable{
    private int id=0;
    private String name="";
    private String description;
    private String URL="";
    private String category="";

    public void setId(int id){
    	this.id=id;
    }
    public int getId(){
    	return this.id;
    }


    public void setName(String name){
    	this.name=name;
    }
    public String getName(){
    	return this.name;
    }

    public void setDescription(String description){
    	this.description=description;
    }
    public String getDescription(){
    	return this.description;
    }

    public void setURL(String URL){
    	this.URL=URL;
    }
    public String getURL(){
    	return this.URL;
    }


    public void setCategory(String category){
    	this.category=category;
    }
    public String getCategory(){
    	return this.category;
    }
}
