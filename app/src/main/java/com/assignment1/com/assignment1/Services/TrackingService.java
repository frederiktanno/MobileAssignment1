package com.assignment1.com.assignment1.Services;

// simulated tracking service by Caspar for MAD s2, 2018
// Usage: add this class to project in appropriate package
// add tracking_data.txt to res/raw folder
// see: TestTrackingService.test() method for example

// NOTE: you may need to explicitly add the import for the generated some.package.R class
// which is based on your package declaration in the manifest

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import com.assignment1.com.assignment1.Models.DistanceMatrixModel;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.R;

public class TrackingService
{
   // PRIVATE PORTION
   private static final String LOG_TAG = TrackingService.class.getName();
   private ArrayList<TrackingModel> trackingList = new ArrayList<>();
   private ArrayList<DistanceMatrixModel>distList = new ArrayList<>();

   private static Context context;
   private DateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a",Locale.getDefault());

   // Singleton
   private TrackingService()
   {
   }


   // check if the source date is with the range of target date +/- minutes and seconds
   private boolean dateInRange(Date source, Date target, int periodMinutes, int periodSeconds)
   {
      Calendar sourceCal = Calendar.getInstance();
      Calendar targetCalStart = Calendar.getInstance();
      Calendar targetCalEnd = Calendar.getInstance();
      // set the calendars for comparison
      sourceCal.setTime(source);
      targetCalStart.setTime(target);
      targetCalEnd.setTime(target);

      // set up start and end range match for mins/secs
      // +/- period minutes/seconds to check
      targetCalStart.set(Calendar.MINUTE, targetCalStart.get(Calendar.MINUTE) - periodMinutes);
      targetCalStart.set(Calendar.SECOND, targetCalStart.get(Calendar.SECOND) - periodSeconds);
      targetCalEnd.set(Calendar.MINUTE, targetCalEnd.get(Calendar.MINUTE) + periodMinutes);
      targetCalEnd.set(Calendar.SECOND, targetCalEnd.get(Calendar.SECOND) + periodMinutes);

      // return if source date in the target range (inclusive of start/end range)
      return sourceCal.equals(targetCalStart) || sourceCal.equals(targetCalEnd)
              || (sourceCal.after(targetCalStart) && sourceCal.before(targetCalEnd));
   }

   // called internally before usage
   public void parseFile(Context context)
   {
      trackingList.clear();

      // resource reference to tracking_data.txt in res/raw/ folder of your project
      // supports trailing comments with //
      try (Scanner scanner = new Scanner(context.getResources().openRawResource(R.raw.tracking_data)))
      {
         // match comma and 0 or more whitespace OR trailing space and newline
         scanner.useDelimiter(",\\s*|\\s*\\n+");
         while (scanner.hasNext())
         {
            TrackingModel TrackingModel = new TrackingModel();
            TrackingModel.setDate(format.parse(scanner.next()));
            //TrackingModel.setDate(DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM,Locale.getDefault()).parse(scanner.next()));
            TrackingModel.setId(Integer.parseInt(scanner.next()));
            TrackingModel.setStop(Integer.parseInt(scanner.next()));
            TrackingModel.setLatitude(Double.parseDouble(scanner.next()));
            String next=scanner.next();
            int commentPos;
            // strip trailing comment
            if((commentPos=next.indexOf("//")) >=0)
               next=next.substring(0, commentPos);
            TrackingModel.setLongitude(Double.parseDouble(next));
            trackingList.add(TrackingModel);
         }
      }
      catch (Resources.NotFoundException e)
      {
      }
      catch (ParseException e)
      {
         Log.e(LOG_TAG, e.toString());
      }
   }

   // singleton support
   private static class LazyHolder
   {
      static final TrackingService INSTANCE = new TrackingService();
   }

   // PUBLIC METHODS

    public ArrayList<TrackingModel>getTrackingList(){
       return this.trackingList;
    }

    public void addDistanceMatrix(DistanceMatrixModel mod){
      this.distList.add(mod);
    }

   private int search(int id){
      int index=0;
      boolean found=false;
      for(int i=0; i<distList.size();i++){//search for tracking data
         Log.i(LOG_TAG,"size: "+distList.size());

         if(id== distList.get(i).getIdTracking()) {
            index = i;
            found=true;
            break;
         }
         else{
            found=false;
         }
      }
      if(found==false){
         return -1;
      }
      return index;
   }

    //get walking time by tracking ID
   public Double getWalkingTime(int id){
      int index = search(id);
      return distList.get(index).getDuration();
   }

   public ArrayList<DistanceMatrixModel> getDistList() {
      return this.distList;
   }

   // singleton
   // thread safe lazy initialisation: see https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
   public static TrackingService getSingletonInstance(Context context)
   {
      TrackingService.context = context;
      return LazyHolder.INSTANCE;
   }

   public void clearData(){
      if(!this.trackingList.isEmpty() || this.trackingList!=null){
         this.trackingList.clear();
      }

   }

   public void setData(ArrayList<TrackingModel>model){
      this.trackingList = model;
   }

   // log contents of file (for testing/logging only)
   public void logAll()
   {
      log(trackingList);
   }

   // log contents of provided list (for testing/logging and example purposes only)
   public void log(List<TrackingModel> trackingList)
   {
      // we reparse file contents for latest data on every call
      parseFile(context);
      for (TrackingModel TrackingModel : trackingList)
      {
         // to prevent this logging issue https://issuetracker.google.com/issues/77305804
         try
         {
            Thread.sleep(1);
         }
         catch (InterruptedException e)
         {
         }
         Log.i(LOG_TAG, TrackingModel.toString());
      }
   }

   // the main method you can call periodically to get data that matches a given date period
   // date +/- period minutes/seconds to check
   public List<TrackingModel> getTrackingModelForTimeRange(Date date, int periodMinutes, int periodSeconds)
   {
      // we reparse file contents for latest data on every call
      parseFile(context);
      List<TrackingModel> returnList = new ArrayList<>();
      for (TrackingModel TrackingModel : trackingList)
         if (dateInRange(TrackingModel.getDate(), date, periodMinutes, periodSeconds))
            returnList.add(TrackingModel);
      return returnList;
   }


}
