package com.assignment1.com.assignment1.DatabaseHandler;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.assignment1.com.assignment1.Models.TrackableModel;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.Models.UserTrackingModel;
import com.assignment1.com.assignment1.Services.CardinalityService;
import com.assignment1.com.assignment1.Services.TrackableService;
import com.assignment1.com.assignment1.Services.TrackingService;
import com.assignment1.com.assignment1.Services.UserTrackingService;
import com.assignment1.com.assignment1.View.MainActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class LoadDatabase extends AsyncTask<Void,Integer,Void> {
    private final String LOG_TAG = this.getClass().getName();
    private MainActivity activity;
    private TrackableService service;
    private TrackingService trackService;
    private UserTrackingService userTrackingService;
    private CardinalityService carService;
    private ArrayList<TrackableModel> trackableList;
    private ArrayList<TrackingModel> trackingList;
    private Context context;
    private DateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a", Locale.getDefault());

    public LoadDatabase(MainActivity activity){
        this.activity=activity;
        this.context = activity.getApplicationContext();
        this.service = TrackableService.getSingletonInstance(this.context);
        this.trackService = TrackingService.getSingletonInstance(this.context);
        this.carService=CardinalityService.getSingletonInstance(this.context);
        this.userTrackingService = UserTrackingService.getSingletonInstance(this.context);

    }

    @Override
    protected void onPreExecute()
    {
    }

    @Override
    protected Void doInBackground(Void...unused){
        Log.i(LOG_TAG,"initializing database");
        DatabaseHelper dbHelper = DatabaseHelper.getSingletonInstance(this.context);
        SQLiteDatabase db = dbHelper.getWriteDb();
        boolean readFromFile = true;
        //check if the tables are empty or not
        if(dbHelper.isTrackableEmpty(db) && dbHelper.isTrackingEmpty(db)){
            Log.i(LOG_TAG,"trackable and tracking tables are empty. Populating with data from txt files");
            //read data from the files

            this.carService.setTrackableCardinality(readFromFile,context);
            this.trackingList=this.trackService.getTrackingList();
            this.trackableList=this.service.getData();
            //put the contents of the trackable list into the database
            for(int i=0; i<trackableList.size();i++){
                dbHelper.addTrackable(trackableList.get(i),db);
            }
            //put the contents of the tracking list into the database
            for(int i=0; i<trackingList.size();i++){
                dbHelper.addTracking(trackingList.get(i),db);
            }
        }
        //if the table is already populated, retrieve data from the database
        else{
            Log.i(LOG_TAG,"fetching data from the database");
            Cursor cursor =dbHelper.getTrackables(db);
            Cursor cursorTracking = dbHelper.getTrackings(db);
            Cursor cursorUserTracking = dbHelper.getUserTrackings(db);
            try{
                ArrayList<TrackableModel>tempTrackable = new ArrayList<>();
                ArrayList<TrackingModel>tempTracking = new ArrayList<>();
                ArrayList<UserTrackingModel>uTracking = new ArrayList<>();

                //this.service.clearData();
                //this.trackService.clearData();
                cursor.moveToFirst();
                cursorUserTracking.moveToFirst();
                cursorTracking.moveToFirst();
                //set the memory model with the retrieved data from the database
                TrackableModel model = new TrackableModel();
                model.setName(cursor.getString(cursor.getColumnIndex("name")));
                model.setId(cursor.getInt(cursor.getColumnIndex("id")));
                model.setCategory(cursor.getString(cursor.getColumnIndex("category")));
                model.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                model.setURL(cursor.getString(cursor.getColumnIndex("URL")));
                tempTrackable.add(model);

                while(cursor.moveToNext()){
                    model = new TrackableModel();
                    model.setName(cursor.getString(cursor.getColumnIndex("name")));
                    model.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    model.setCategory(cursor.getString(cursor.getColumnIndex("category")));
                    model.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                    model.setURL(cursor.getString(cursor.getColumnIndex("URL")));
                    tempTrackable.add(model);
                }
                Log.i(LOG_TAG,"adding data into memory model");
                this.service.setData(tempTrackable);
                try{
                    TrackingModel trackingmodel = new TrackingModel();
                    trackingmodel.setIdTracking(cursorTracking.getInt(cursorTracking.getColumnIndex("id")));
                    trackingmodel.setDate(format.parse(cursorTracking.getString(cursorTracking.getColumnIndex("date"))));
                    trackingmodel.setLongitude(cursorTracking.getDouble(cursorTracking.getColumnIndex("longitude")));
                    trackingmodel.setLatitude(cursorTracking.getDouble(cursorTracking.getColumnIndex("latitude")));
                    trackingmodel.setStop(cursorTracking.getInt(cursorTracking.getColumnIndex("stop")));
                    trackingmodel.setId(cursorTracking.getInt(cursorTracking.getColumnIndex("idTrackable")));
                    tempTracking.add(trackingmodel);
                    //insert tracking data and user tracking data into memory model
                    while(cursorTracking.moveToNext()){
                        trackingmodel = new TrackingModel();
                        trackingmodel.setIdTracking(cursorTracking.getInt(cursorTracking.getColumnIndex("id")));
                        trackingmodel.setDate(format.parse(cursorTracking.getString(cursorTracking.getColumnIndex("date"))));
                        trackingmodel.setLongitude(cursorTracking.getDouble(cursorTracking.getColumnIndex("longitude")));
                        trackingmodel.setLatitude(cursorTracking.getDouble(cursorTracking.getColumnIndex("latitude")));
                        trackingmodel.setStop(cursorTracking.getInt(cursorTracking.getColumnIndex("stop")));
                        trackingmodel.setId(cursorTracking.getInt(cursorTracking.getColumnIndex("idTrackable")));
                        tempTracking.add(trackingmodel);
                    }
                    Log.i(LOG_TAG,"adding tracking data into memory model");
                    this.trackService.setData(tempTracking);
                }
                catch(ParseException ex){
                    Log.e(LOG_TAG,ex.toString());
                    return null;
                }


                //if the user tracking table is populated and user tracking size is empty, read from database
                if(cursorUserTracking.getCount() > 0 && this.userTrackingService.getUserTrackingListSize() == 0){
                    Log.i(LOG_TAG,cursorUserTracking.getCount()+"");
                    try{
                        if(this.userTrackingService.getUserTrackingListSize() != cursorUserTracking.getCount()){
                            this.userTrackingService.clearData();
                        }

                        UserTrackingModel umodel = new UserTrackingModel();
                        //create a new model object
                        umodel.setTrackableId(cursorUserTracking.getInt(cursorUserTracking.getColumnIndex("idTrackable")));
                        umodel.setTitle(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("title")));
                        umodel.setMeetingTime(format.parse(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("meetTime"))));
                        umodel.setStartTime(format.parse(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("startTime"))));
                        umodel.setEndTime(format.parse(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("endTime"))));
                        umodel.setCurrentLocation(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("currLocation")));
                        umodel.setMeetingLocation(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("meetLocation")));
                        //add the new model object to the temporary memory model list
                        uTracking.add(umodel);
                        while(cursorUserTracking.moveToNext()){
                            umodel = new UserTrackingModel();
                            umodel.setTrackableId(cursorUserTracking.getInt(cursorUserTracking.getColumnIndex("idTrackable")));
                            umodel.setTitle(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("title")));
                            umodel.setMeetingTime(format.parse(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("meetTime"))));
                            umodel.setStartTime(format.parse(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("startTime"))));
                            umodel.setEndTime(format.parse(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("endTime"))));
                            umodel.setCurrentLocation(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("currLocation")));
                            umodel.setMeetingLocation(cursorUserTracking.getString(cursorUserTracking.getColumnIndex("meetLocation")));
                            uTracking.add(umodel);
                        }
                        Log.i(LOG_TAG,"adding user tracking data into memory model");
                        this.userTrackingService.setData(uTracking);
                    }
                    catch(ParseException ex){
                        Log.e(LOG_TAG,ex.toString());
                        return null;
                    }
                }

            }
            finally{
                readFromFile = false;
                this.carService.setTrackableCardinality(readFromFile,this.context);
                cursor.close();
                cursorTracking.close();
                cursorUserTracking.close();
            }
        }
        //dbHelper.closeDatabase();
        publishProgress(1);

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress)
    {
        Log.i(LOG_TAG,"database populated");
    }
}
