package com.assignment1.com.assignment1.View;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.assignment1.com.assignment1.Controller.DistanceMatrixController;
import com.assignment1.com.assignment1.Models.DistanceMatrixModel;
import com.assignment1.com.assignment1.Models.LocationModel;
import com.assignment1.com.assignment1.Services.TrackingService;
import com.assignment1.com.assignment1.Services.UserTrackingService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.assignment1.com.assignment1.Controller.DetailActivityListener;
import com.assignment1.com.assignment1.DatabaseHandler.SaveDatabase;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback{
    FloatingActionButton fab;
    private Boolean locationPermission = false;
    private ArrayList<TrackingModel> trackingData;
    private ArrayList<TrackingModel>fullTracking;
    private HashMap<Marker, TrackingModel> markerMeta = new HashMap<>();
    private final String LOG_TAG= this.getClass().getName();
    private static final int ERROR_DIALOG_REQUEST = 9001;
    private static View view;
    private LocationModel model = new LocationModel();
    private UserTrackingService service;
    private TrackingService trackingService;
    FusedLocationProviderClient mFusedLocationProviderClient;
    int id;
    String name;
    private SaveDatabase saveDb;
    final private static String TOKEN  ="AIzaSyCsALZdkNWJHRopoVTigCV8VI7OWDbODZg";
    GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        service = UserTrackingService.getSingletonInstance(this);
        trackingService = TrackingService.getSingletonInstance(this);
        if (savedInstanceState != null) {
            System.out.println("oldData");
            trackingData = (ArrayList<TrackingModel>) savedInstanceState.getSerializable("trackableTracking");
            //title.setText(savedInstanceState.getString("title"));
            name = savedInstanceState.getString("title");
            id = savedInstanceState.getInt("trackableId");

        } else {
            System.out.println("NewData");
            trackingData = (ArrayList<TrackingModel>) getIntent().getSerializableExtra("trackableTracking");

            //title.setText(getIntent().getStringExtra("title"));
            name = getIntent().getStringExtra("title");
            id = getIntent().getIntExtra("trackableId", 0);
        }
        //Set Map
        if (isServiceOK()){
            getLocationPermission();
        }



        //Set Action bar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Setup Spinner
        //setSpinner();

        //setup fab
        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new DetailActivityListener.detailController(this,trackingData,this.id,this.name));

    }

    @Override
    protected void onStop(){
        super.onStop();
        saveDb = new SaveDatabase(this);
        saveDb.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("trackableTracking",outState.getSerializable("trackableTracking"));
        outState.putInt("trackableId",id);
        outState.putString("title",name);
        super.onSaveInstanceState(outState);
    }

    //Check whether Google Service is ok or not
    public boolean isServiceOK(){
        Log.d(LOG_TAG, "isServiceOK: checking google services location");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(DetailActivity.this);
        if(available == ConnectionResult.SUCCESS){
            Log.d(LOG_TAG, "isServiceOK: Google Play service is working");
            return true;
        }
        else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            Log.d(LOG_TAG, "Error Occured, we are able to fix this");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(DetailActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
            return false;
        }
        else{
            Toast.makeText(this, "Couldn't make map request", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    //Get location permission from the user permission
    private void getLocationPermission(){
        System.out.println(">>>>>>>>>>>>>>>>.GETTING LOCATION PERMISSION");
        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                locationPermission = true;
                //if permission is granted already, initialize map
                initMap();
            }else {
                //else ask permission
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>HOI");
                ActivityCompat.requestPermissions(this, permission, 1234);
            }
        }
        else{
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>HOI");
            ActivityCompat.requestPermissions(this, permission, 1234);
        }
    }

    //If there's no permission, ask the phone
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationPermission = false;
        switch (requestCode){
            case 1234:{
                if(grantResults.length > 0){
                    for(int i = 0; i < grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            locationPermission = false;
                            return;
                        }
                    }
                    locationPermission = true;
                    Toast.makeText(this, "Map Ready!", Toast.LENGTH_SHORT).show();
                    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>. MAP READY");
                    //if permission all granted, initialize map
                    initMap();
                }
            }
        }
    }

    //Initialize map
    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(DetailActivity.this);
    }

    //When map ready
    @Override
    public void onMapReady(GoogleMap googleMap) {
        boolean isStopping = false;
        Marker marker;
        mMap = googleMap;

        if(locationPermission){
            //Get device location
            getDeviceLocation();
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                return;
            }
            mMap.setMyLocationEnabled(true);
        }

        for (int i= 0; i<trackingData.size(); i++){
            if(trackingData.get(i).getStop() > 0){
                isStopping = true;
                marker = createMarkers(trackingData.get(i).getLongitude(), trackingData.get(i).getLatitude(),name, isStopping);
                markerMeta.put(marker,trackingData.get(i));
            }
            else{
                isStopping = false;
                createMarkers(trackingData.get(i).getLongitude(), trackingData.get(i).getLatitude(),name, isStopping);
            }
        }
        Toast.makeText(this, "Please tap on the marker to book an appointment (Blue: Not stopping | Red: Stopping)", Toast.LENGTH_LONG).show();
        googleMap.setOnMarkerClickListener(new DetailActivityListener.detailMarkerController(this,trackingData,this.id,this.name, this.markerMeta));
    }

    //getting the device location
    public void getDeviceLocation() {
        System.out.println(">>>>>>>>>>>>>>>>>>GET DEVICE LOCATION");
        this.locationPermission=locationPermission;
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (locationPermission) {
                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        //if done getting location
                        if (task.isSuccessful()) {
                            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>FOUND LOCATION");
                            Location currentLocation = (Location) task.getResult();
                            //set position to model
                            model.setLatitude(currentLocation.getLatitude());
                            model.setLongitude(currentLocation.getLongitude());
                            service.setLocation(currentLocation.getLatitude(),currentLocation.getLongitude());
                            //move camera
                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15f);
                            //set distance matrix
                            setupDistanceMatrix();

                        } else {
                            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>UNABLE TO GET CURRENT LOCATION");
                            Toast.makeText(DetailActivity.this, "Unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            System.out.println("Security exception:  " + e.getMessage());
        }
    }

    //Move camera to the map
    private void moveCamera(LatLng latLng, float zoom){
        Log.i(LOG_TAG,">>>>>>>>>>>>>>>>>>>Moving camera to lat " + latLng.latitude + ",long " + latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    protected Marker createMarkers(double longitude, double latitude, String name, Boolean color){
        if (color == true)
            return mMap.addMarker(new MarkerOptions().position(new LatLng(latitude,longitude)).title(name + " " + latitude + " | " + longitude));
        else
            return mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("(NOT STOPPING) " + name).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
    }

    //get distance matrix from google api
    private void setupDistanceMatrix(){
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>Location Model : Location =" + model.getLongitude() + "|" + model.getLatitude());
        fullTracking = this.trackingService.getTrackingList();
        DistanceMatrixModel mod;
        Log.i(LOG_TAG,"Full tracking size: " + fullTracking.size() + "");
        for (int i = 0; i<fullTracking.size();i++){
            if (fullTracking.get(i).getStop()>0){
                mod = new DistanceMatrixModel();//create a new distance matrix model
                mod.setIdTrackable(fullTracking.get(i).getId());//set trackable ID
                mod.setIdTracking(fullTracking.get(i).getIdTracking());//set tracking ID
                //url to be sent to google
                String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                        model.getLatitude() + "," + model.getLongitude() + "&destinations=" +
                        fullTracking.get(i).getLatitude() + "," + fullTracking.get(i).getLongitude() +
                        "&mode=walking&language=fr-FR&avoid=tolls&key=" + TOKEN;
                //controller for distance matrix
                new DistanceMatrixController(this,mod).execute(url);
            }
        }
    }
}
