package com.assignment1.com.assignment1.Services;
import android.content.Context;
import android.util.Log;

//import com.assignment1.com.assignment1.Models.Cardinality;
import com.assignment1.com.assignment1.Models.TrackableModel;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.Models.UserTrackingModel;
import com.assignment1.com.assignment1.Services.TrackableService;
import com.assignment1.com.assignment1.Services.TrackingService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

public class CardinalityService {

    private static Context context;
    private TrackableService trackableService;
    private TrackingService trackingService;
    private ArrayList<UserTrackingModel> userTrackingList;
    private ArrayList<TrackableModel> trackableData=new ArrayList<>();
    private ArrayList<TrackingModel> trackingData = new ArrayList<>();
    private ArrayList<Integer>trackableId = new ArrayList<>();
    private static final String LOG_TAG=UserTrackingService.class.getName();
    private LinkedHashMap<Integer,ArrayList<TrackingModel>> cardinality = new LinkedHashMap<>();

    class SortId implements Comparator<TrackingModel> {
        public int compare(TrackingModel a, TrackingModel b){return a.getId()- b.getId();}

    }

    // Singleton
    private CardinalityService()
    {

    }

    // singleton support
    private static class LazyHolder
    {
        static final CardinalityService INSTANCE = new CardinalityService();
    }

    public static CardinalityService getSingletonInstance(Context context){
        CardinalityService.context=context;
        return LazyHolder.INSTANCE;
    }

    /*
        This function sets up the relationship between trackable and tracking data
        Similar to how MySQL treats primary key-foreign key relationship
    */
    public void setTrackableCardinality(boolean readFromFile, Context context){
        cardinality.clear();
        initTrackableService(context);
        initTrackingService(context);
        //read files for constant update on data
        if(readFromFile==true){
            trackableService.readFile(context);
            trackingService.parseFile(context);
        }


        //get list of trackables and tracking data
        this.trackableData = trackableService.getData();
        this.trackingData = trackingService.getTrackingList();
        //sort tracking data by Id
        Collections.sort(trackingData,new SortId());
        //get id of trackables from trackableData
        for(int i=0; i<trackableData.size(); i++){

            trackableId.add(trackableData.get(i).getId());
        }
        //insert trackings into a hash map according to its userID
        //the hash map is a collection of relationships between trackableID and tracking's trackableID
        for(int i=0; i<trackableId.size(); i++){

            cardinality.put(i+1,searchTrackings(trackableId.get(i)));
        }

    }

    private ArrayList<TrackingModel>searchTrackings(int id){
        ArrayList<TrackingModel> filteredTracking = new ArrayList<>();
        for(int i=0; i<trackingData.size(); i++){
            if(trackingData.get(i).getId()==id){
                filteredTracking.add(trackingData.get(i));

            }
        }
        return filteredTracking;
    }

    private void initTrackableService(Context context){
        this.trackableService=TrackableService.getSingletonInstance(context);
    }

    private void initTrackingService(Context context){
        this.trackingService=TrackingService.getSingletonInstance(context);
    }

    public ArrayList<TrackingModel> getTrackingDataById(int id) {
        return cardinality.get(id);
    }
}
