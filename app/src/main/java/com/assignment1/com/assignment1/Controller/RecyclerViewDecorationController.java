package com.assignment1.com.assignment1.Controller;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.assignment1.com.assignment1.R;

public class RecyclerViewDecorationController extends RecyclerView.ItemDecoration {
    private int space;

    public RecyclerViewDecorationController(int space) {
        this.space = space;
    }

    public static RecyclerViewDecorationController createDecoration(Context context) {

        int spacingInPixels = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
        return new RecyclerViewDecorationController(spacingInPixels);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.top = space;
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
    }
}
