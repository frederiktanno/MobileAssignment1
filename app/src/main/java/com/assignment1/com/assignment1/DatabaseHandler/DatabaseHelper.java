package com.assignment1.com.assignment1.DatabaseHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.assignment1.com.assignment1.Models.TrackableModel;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.Models.UserTrackingModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DatabaseHelper extends SQLiteOpenHelper {

    //database helper class for handling database initialisation and provides general database functionality

    private static final String LOG_TAG=DatabaseHelper.class.getName();
    //Database info
    private static final String dbName = "MAD.db";
    private static final int version = 1;
    private int dbOpenCounter=0;

    //Table Names
    private static final String trackable = "Trackables";
    private static final String tracking = "Trackings";
    private static final String userTracking = "UserTrackings";

    //Trackable column names
    private static final String id="id";//data type = int, primary key
    private static final String name="name";
    private static final String description="description";
    private static final String URL="URL";
    private static final String category="category";

    //Tracking column names
    private static final String idTracking="id";//data type = int, primary key
    private static final String idTrackable = "idTrackable";// foreign key
    private static final String date="date"; //data type = date
    private static final String stop="stop";//data type = int
    private static final String latitude="latitude";//data type = Double
    private static final String longitude="longitude";//data type = Double

    //user tracking column names
    private static final String idUserTracking = "id";//data type = int, primary key
    private static final String idUserTrackable = "idTrackable";//foreign key
    private static final String title = "title";
    private static final String startTime = "startTime";//data type = date
    private static final String endTime = "endTime";//data type = date
    private static final String meetTime = "meetTime";//data type = date
    private static final String currentLocation = "currLocation";//data type = String
    private static final String meetLocation = "meetLocation";
    private static final String walkingTime="walkingTime";

    private SQLiteDatabase db=null;
    private Context context;

    public static DatabaseHelper singletonInstance;

    //singleton support
    public static DatabaseHelper getSingletonInstance(Context context){
        if(singletonInstance==null){
            Log.i(LOG_TAG,"getting db instance");
            singletonInstance = new DatabaseHelper(context.getApplicationContext());

        }
        return singletonInstance;
    }

    private DatabaseHelper(Context context){
        super(context,dbName,null,version);
        this.context=context;

    }

    // Called when the database connection is being configured.
    @Override
    public void onConfigure(SQLiteDatabase db){
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTrackingTable = "CREATE TABLE " + tracking +
                "(" +
                idTracking + " INTEGER PRIMARY KEY AUTOINCREMENT," + // Define a primary key
                idTrackable + " INTEGER REFERENCES " + trackable + "," + // Define a foreign key
                date + " TEXT," +
                stop + " INTEGER,"+
                latitude + " DOUBLE,"+
                longitude + " DOUBLE"+
                ")";

        String createTrackableTable = "CREATE TABLE " + trackable +
                "(" +
                id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                name + " TEXT," +
                description + " TEXT," +
                URL + " TEXT,"+
                category + " TEXT"+
                ")";

        String createUserTrackingTable = "CREATE TABLE " + userTracking +
                "(" +
                idUserTracking+ " INTEGER PRIMARY KEY AUTOINCREMENT," + // Define a primary key
                idUserTrackable + " INTEGER REFERENCES " + trackable + "," + // Define a foreign key
                title + " TEXT," +
                startTime + " TEXT,"+
                endTime+ " TEXT,"+
                meetTime+ " TEXT,"+
                currentLocation+ " TEXT,"+
                meetLocation+ " TEXT"+
                ")";
        try{
            Log.i(LOG_TAG,createTrackableTable);
            Log.i(LOG_TAG,createTrackingTable);
            Log.i(LOG_TAG,createUserTrackingTable);
            db.execSQL(createTrackableTable);
            db.execSQL(createTrackingTable);
            db.execSQL(createUserTrackingTable);
        }
        catch(SQLiteException ex){
            Log.e(LOG_TAG,ex.toString());
        }

    }

    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + trackable);
            db.execSQL("DROP TABLE IF EXISTS " + tracking);
            db.execSQL("DROP TABLE IF EXISTS " + userTracking);
            onCreate(db);
        }
    }

    public Boolean isTrackableEmpty(SQLiteDatabase db){

        String count = "SELECT count(*) from "+trackable;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0){
            return false;
        }
        else{
            return true;
        }
    }

    public Boolean isTrackingEmpty(SQLiteDatabase db){

        String count = "SELECT count(*) from "+tracking;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0){
            return false;
        }
        else{
            return true;
        }
    }

    public Boolean isUserTrackingEmpty(SQLiteDatabase db){
        String count = "SELECT count(*) from "+userTracking;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0){
            return false;
        }
        else{
            return true;
        }
    }

    //get a writable db instance

    public SQLiteDatabase getWriteDb(){
        if (db==null) {
            db=getWritableDatabase();
        }

        return(db);
    }

    //insert into trackable table

    public void addTrackable(TrackableModel trackableData, SQLiteDatabase db){
        db.beginTransaction();
        try{
            ContentValues values = new ContentValues();
            values.put(name,trackableData.getName());
            values.put(description,trackableData.getDescription());
            values.put(URL,trackableData.getURL());
            values.put(category,trackableData.getCategory());

            db.insertOrThrow(trackable, null, values);
            db.setTransactionSuccessful();
        }
        catch(Exception e){
            Log.e(LOG_TAG,e.toString());
        }
        finally{
            db.endTransaction();
        }
    }

    public void addTracking(TrackingModel trackingData, SQLiteDatabase db){
        DateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a", Locale.getDefault());
        db.beginTransaction();
        try{
            ContentValues values = new ContentValues();
            values.put(idTrackable,trackingData.getId());
            values.put(date,format.format(trackingData.getDate()));
            values.put(stop,trackingData.getStop());
            values.put(longitude,trackingData.getLongitude());
            values.put(latitude,trackingData.getLatitude());

            db.insertOrThrow(tracking, null, values);
            db.setTransactionSuccessful();
        }
        catch(Exception e){
            Log.e(LOG_TAG,e.toString());
        }
        finally{
            db.endTransaction();
        }
    }

    public void addUserTracking(UserTrackingModel data, SQLiteDatabase db){

        DateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a", Locale.getDefault());
        db.beginTransaction();
        try{
            ContentValues values = new ContentValues();
            values.put(idUserTrackable,data.getTrackableId());
            values.put(title,data.getTitle());
            values.put(startTime,format.format(data.getStartTime()));
            values.put(endTime,format.format(data.getEndTime()));
            values.put(meetTime,format.format(data.getMeetingTime()));
            values.put(currentLocation,data.getCurrentLocation());
            values.put(meetLocation,data.getMeetingLocation());
            db.insertOrThrow(userTracking, null, values);
            db.setTransactionSuccessful();
        }
        catch(Exception e){
            Log.e(LOG_TAG,e.toString());
        }
        finally{
            db.endTransaction();
        }
    }

    public Cursor getTrackables(SQLiteDatabase db){
        return db.query(trackable,null,null,null,null,null,id);
    }

    public Cursor getTrackings(SQLiteDatabase db){
        return db.query(tracking,null,null,null,null,null,idTracking);
    }

    public Cursor getUserTrackings(SQLiteDatabase db){
        return db.query(userTracking,null,null,null,null,null,idUserTracking);
    }

    public void closeDatabase(){
        db.close();
    }

    public int countUserTrackings(SQLiteDatabase db){

        Cursor cursor = getUserTrackings(db);
        return cursor.getCount();
    }

    public void clearTable(String tablename){
        db.execSQL("DELETE FROM "+tablename);
    }
    }

