package com.assignment1.com.assignment1.Adapter;

import com.assignment1.com.assignment1.Controller.DistanceMatrixController;
import com.assignment1.com.assignment1.Controller.MainActivityListener;
import com.assignment1.com.assignment1.DatabaseHandler.DatabaseHelper;
import com.assignment1.com.assignment1.Models.LocationModel;
import com.assignment1.com.assignment1.Models.TrackableModel;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.R;
import com.assignment1.com.assignment1.Services.CardinalityService;
import com.assignment1.com.assignment1.Services.TrackableService;
import com.assignment1.com.assignment1.Services.TrackingService;
import com.assignment1.com.assignment1.Services.UserTrackingService;
import com.assignment1.com.assignment1.View.MainActivity;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.view.View;
import android.content.Context;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainActivityListener.ViewHolder>{
    private TrackableService service;
    private TrackingService trackService;
    private UserTrackingService userTrackingService;
    private CardinalityService carService;
    //private ArrayList<TrackableModel> trackableList;
    private ArrayList<TrackableModel> trackableFilteredList;
    private ArrayList<TrackingModel> trackingList;
    private LocationModel model;
    private Context context;
    private DistanceMatrixController distanceMatrixController;
    private static final String LOG_TAG=MainAdapter.class.getName();

    public MainAdapter(Context context){
        this.context=context;
        this.service=TrackableService.getSingletonInstance(context);
        this.userTrackingService = UserTrackingService.getSingletonInstance(context);
        this.trackService=TrackingService.getSingletonInstance(context);
        this.carService=CardinalityService.getSingletonInstance(context);
        //this.service.readFile(context);
        //this.trackService.parseFile(context);

        //this.trackableList=this.service.getData();
        this.trackingList=this.trackService.getTrackingList();
        this.trackableFilteredList=this.service.getData();
        this.model=this.userTrackingService.getLocation();

    }

    @Override
    public MainActivityListener.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v =LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_layout, parent, false);
        MainActivityListener.ViewHolder vh = new MainActivityListener.ViewHolder(v, this.context, this.trackableFilteredList);

        return vh;
    }

    @Override
    public void onBindViewHolder(MainActivityListener.ViewHolder holder, int position) {


        holder.text.setText(trackableFilteredList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return trackableFilteredList.size();
    }
    /*
    public static class TruckViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        //insert Constructor
        public TruckViewHolder(View view){
            view.setOnClickListener(this);
        }
        public void onClick(View v){
            //pass data to next activity
            //Go to the next activity
        }

    }*/

    public ArrayList<TrackableModel> filter(String category){
        trackableFilteredList.clear();
        trackableFilteredList = service.getDataByCategory(category);
        return trackableFilteredList;
    }

    public ArrayList<TrackableModel> getAll(){
        trackableFilteredList = service.getData();
        return trackableFilteredList;
    }


}
