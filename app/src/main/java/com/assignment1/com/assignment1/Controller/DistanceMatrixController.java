package com.assignment1.com.assignment1.Controller;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.assignment1.com.assignment1.Models.DistanceMatrixModel;
import com.assignment1.com.assignment1.Services.TrackingService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DistanceMatrixController extends AsyncTask<String, Void, String>{
    Context context;
    //Query  dm;
    DistanceMatrixModel model;
    TrackingService service;
    private static String LOG_TAG = DistanceMatrixController.class.getName();

    public DistanceMatrixController(Context context, DistanceMatrixModel model){
        this.context = context;
        this.model=model;
        this.service = TrackingService.getSingletonInstance(context);
        //dm = (Query) context;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s!=null){
            //dm.setDouble(s);
            String res[]=s.split(",");
            Double minutes = Double.parseDouble(res[0])/60;
            int distance = Integer.parseInt(res[1]);
            Log.i(LOG_TAG,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>Minutes: " + minutes + "minutes");
            Log.i(LOG_TAG,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>distance " + distance + "meter");
            this.model.setDistance(distance);
            this.model.setDuration(minutes);
            this.service.addDistanceMatrix(this.model);
        }
        else{
            Toast.makeText(context, "URL Formatting error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        try{
            URL url = new URL(params[0]);
            System.out.println(">>>>>>>>>>>>URL :" + url);
            HttpURLConnection  connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int status = connection.getResponseCode();

            if(status == HttpURLConnection.HTTP_OK){
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();
                while(line!=null)
                {
                    sb.append(line);
                    line=br.readLine();
                }
                String json = sb.toString();
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>JSON: " + json);
                JSONObject root = new JSONObject(json);
                JSONArray array_rows = root.getJSONArray("rows");
                JSONObject object_rows = array_rows.getJSONObject(0);
                JSONArray array_elements = object_rows.getJSONArray("elements");
                JSONObject  object_elements = array_elements.getJSONObject(0);
                JSONObject object_duration = object_elements.getJSONObject("duration");
                JSONObject object_distance = object_elements.getJSONObject("distance");

                Log.d("JSON","object_duration:"+object_duration);
                return object_duration.getString("value")+","+object_distance.getString("value");
            }
            else{
                Log.e(LOG_TAG,status+"");
                return status+"";
            }

        }
        catch(MalformedURLException e){
            System.out.println("doInBackGround : MalformedURLException");
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
