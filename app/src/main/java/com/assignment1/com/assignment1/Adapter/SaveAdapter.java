package com.assignment1.com.assignment1.Adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.Models.UserTrackingModel;
import com.assignment1.com.assignment1.R;
import com.assignment1.com.assignment1.Services.UserTrackingService;

import java.util.ArrayList;

public class SaveAdapter extends ArrayAdapter {
    Context context;
    ArrayList<UserTrackingModel>trackings;
    private UserTrackingService service;
    private int id;
    public SaveAdapter(Context context, int idLayout, ArrayList<UserTrackingModel> trackings, int id){
        super(context, R.layout.booking_layout,trackings);//ganti layoutnya
        this.context=context;
        this.trackings=trackings;
        this.id=id;
        this.service= UserTrackingService.getSingletonInstance(this.context);
    }

}
