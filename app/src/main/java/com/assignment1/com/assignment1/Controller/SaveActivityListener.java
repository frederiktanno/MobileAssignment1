package com.assignment1.com.assignment1.Controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.assignment1.com.assignment1.Models.LocationModel;
import com.assignment1.com.assignment1.Services.TrackingService;
import com.assignment1.com.assignment1.Services.UserTrackingService;
import com.assignment1.com.assignment1.View.MainActivity;

import java.util.ArrayList;

public class SaveActivityListener {

    public static class detailController implements View.OnClickListener{
        private Context context;
        private final String LOG_TAG = this.getClass().getName();
        private UserTrackingService service;
        private TrackingService trackingService;
        private TextInputEditText nameMeeting, time;
        private TextView stop, date;
        private String location;
        private ArrayList<String>data;
        private int id;
        private String check;
        private Double minutes;
        private LocationModel loc;

        public detailController(Context context, TextView stop, String location, TextView date,
                                TextInputEditText nameMeeting, TextInputEditText time, int id,Double walk){
            this.context =  context;
            this.stop = stop;
            this.location = location;
            this.nameMeeting = nameMeeting;
            this.time = time;
            this.date = date;
            this.minutes = walk;
            data=new ArrayList<>();
            this.id=id;
            service=UserTrackingService.getSingletonInstance(context);
            trackingService = TrackingService.getSingletonInstance(context);
            this.loc = service.getLocation();

        }

        @Override
        public void onClick(View v) {
            Snackbar.make(v, "You pressed Save Button", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            String stopData = stop.getText().toString();
            stopData=stopData.replace("minutes","").trim();
            String nameMeetingData = nameMeeting.getEditableText().toString();
            data.add(nameMeetingData);
            data.add(date.getText().toString());//get start date
            data.add(stopData);//get stopping time
            String timeData = time.getEditableText().toString();
            data.add(timeData);//meeting time
            String locationData = location;//meeting location
            data.add(loc.getLatitude().toString()+", "+loc.getLongitude().toString());
            Log.i(LOG_TAG,loc.getLatitude().toString()+", "+loc.getLongitude().toString());
            data.add(locationData);
            Log.i(LOG_TAG,"id: "+id);
            check=service.saveData(data,id);
            //sends input to be processed and persisted
            if(check.equals("parse error")){

                //service=UserTrackingService.getSingletonInstance(context);
                data.clear();
                //handle errors here
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);

                builder.setTitle("Alert")
                        .setMessage("Please Insert the Correct date Format")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
            else if(check.equals("Invalid date")){
                data.clear();
                //handle errors here
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);

                builder.setTitle("Alert")
                        .setMessage("Invalid Date value. Please insert date between the stopping time")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
            else if(check.equals("OK")){

                Intent intent= new Intent(this.context, MainActivity.class);//creates a new Intent to refer to an activity
                this.context.startActivity(intent);
            }
        }
    }
}
