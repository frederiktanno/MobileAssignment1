package com.assignment1.com.assignment1.View;

import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.assignment1.com.assignment1.Adapter.SaveAdapter;
import com.assignment1.com.assignment1.Controller.SaveActivityListener;
import com.assignment1.com.assignment1.DatabaseHandler.SaveDatabase;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.R;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class SaveActivity extends AppCompatActivity {
    private TextInputEditText nameMeeting, time;
    private Spinner spinner;
    private Button save;
    private TextView stop, date, walkingTime;
    private SaveAdapter adapter;
    private TrackingModel trackingData;
    private String selectedDate;
    private Double walk;
    private String location;

    private int id;
    private SaveDatabase saveDb;
    SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a", Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);
        date = (TextView) findViewById(R.id.txtSaveStop);
        stop = (TextView)findViewById(R.id.txtData);
        walkingTime = (TextView)findViewById(R.id.txtWalkingData);
        //location = (TextView)findViewById(R.id.txtLocationData);
        nameMeeting = (TextInputEditText)findViewById(R.id.insertTitleTextInput);
        time = (TextInputEditText)findViewById(R.id.insertMeetingTimeTextInput);
        if(savedInstanceState==null){
            this.trackingData=(TrackingModel)getIntent().getSerializableExtra("trackableTracking");
            this.id=getIntent().getIntExtra("id",0);
            this.walk = getIntent().getDoubleExtra("walkingTime",0);
            this.walk = round(walk,2);
        }
        else{
            this.trackingData=(TrackingModel) savedInstanceState.getSerializable("trackableTracking");
            this.id=savedInstanceState.getInt("id",0);
            this.walk = savedInstanceState.getDouble("walkingTime",0);
            this.walk = round(walk,2);
        }

        //Set Action bar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add a Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //set Text View
        date.setText(format.format(trackingData.getDate()));
        walkingTime.setText(String.valueOf(walk) +" minutes");
        stop.setText(String.valueOf(trackingData.getStop()) + " minutes");
        //location.setText(trackingData.getLatitude() + ", " + trackingData.getLongitude());
        location = trackingData.getLatitude() + ", " + trackingData.getLongitude();
        //set Spinner
//        setSpinner();
//        spinner = (Spinner) findViewById(R.id.spinnerStop);


        //set Button
        save = (Button)findViewById(R.id.btnSave);
        save.setOnClickListener(new SaveActivityListener.detailController(this,stop,location,date,nameMeeting,time,id,walk));
        //setButton();
    }
    @Override
    protected void onStop(){
        super.onStop();
        saveDb = new SaveDatabase(this);
        saveDb.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    //round up the decimal value with x decimal places
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
