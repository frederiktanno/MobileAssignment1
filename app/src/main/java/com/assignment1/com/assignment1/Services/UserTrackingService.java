package com.assignment1.com.assignment1.Services;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.assignment1.com.assignment1.Models.DistanceMatrixModel;
import com.assignment1.com.assignment1.Models.LocationModel;
import com.assignment1.com.assignment1.Models.UserTrackingModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/*
    This class contains functions of logical operations for user's list of added tracking data
    as well as mapping trackables with their tracking data
*/

public class  UserTrackingService {

    private static Context context;
    private ArrayList<UserTrackingModel>userTrackingList=new ArrayList<>();
    private LocationModel loc = new LocationModel();
    private static final String LOG_TAG=UserTrackingService.class.getName();
    //private LinkedHashMap<Integer,ArrayList<String>> cardinality = new LinkedHashMap<>();
    //private DateFormat format=new SimpleDateFormat("EEE M dd HH:mm:ss z yyyy");
    private DateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a",Locale.getDefault());
    public int userTrackingListSize = 0;
    // Singleton
    private UserTrackingService()
    {
    }

    // singleton support
    private static class LazyHolder
    {
        static final UserTrackingService INSTANCE = new UserTrackingService();
    }

    public static UserTrackingService getSingletonInstance(Context context){
        UserTrackingService.context=context;
        return LazyHolder.INSTANCE;
    }

    private boolean dateInRange(Date source, Date target, int periodMinutes, int periodSeconds)
    {
        Calendar sourceCal = Calendar.getInstance();
        Calendar targetCalStart = Calendar.getInstance();
        Calendar targetCalEnd = Calendar.getInstance();
        // set the calendars for comparison
        sourceCal.setTime(source);
        targetCalStart.setTime(target);
        targetCalEnd.setTime(target);
        // set up start and end range match for mins/secs
        // +/- period minutes/seconds to check
        targetCalStart.set(Calendar.MINUTE, targetCalStart.get(Calendar.MINUTE) - periodMinutes);
        targetCalStart.set(Calendar.SECOND, targetCalStart.get(Calendar.SECOND) - periodSeconds);
        targetCalEnd.set(Calendar.MINUTE, targetCalEnd.get(Calendar.MINUTE) + periodMinutes);
        targetCalEnd.set(Calendar.SECOND, targetCalEnd.get(Calendar.SECOND) + periodMinutes);

        // return if source date in the target range (inclusive of start/end range)
        return sourceCal.equals(targetCalStart) || sourceCal.equals(targetCalEnd)
                || (sourceCal.after(targetCalStart) && sourceCal.before(targetCalEnd));
    }

    public void setLocation(Double latitude, Double longitude){
        this.loc.setLatitude(latitude);
        this.loc.setLongitude(longitude);
    }


    public LocationModel getLocation(){
        return this.loc;
    }

    /*public void readFile(Context context){
        //this.context=context;
        userTrackingList.clear();
        try (Scanner scan=new Scanner(context.getResources().openRawResource(R.raw.food_truck_data))){
            scan.useDelimiter(";\\s*|\\s*\\n+");//uses the ; symbol as delimiter
            while(scan.hasNext()){
                UserTrackingModel model= new UserTrackingModel();
                model.setId(scan.next());
                model.setTrackableId(Integer.parseInt(scan.next()));
                model.setStartTime(format.parse(scan.next()));
                model.setEndTime(format.parse(scan.next()));
                model.setCurrentLocation(scan.next());
                model.setMeetingLocation(scan.next());
                userTrackingList.add(model);
            }
            scan.close();
        }
        catch(Resources.NotFoundException ex){
            Log.e(LOG_TAG, ex.toString());
        }
        catch(ParseException ex){
            Log.e(LOG_TAG, ex.toString());
        }

    }*/

    public String saveData(ArrayList<String>postData, int id){
        try{
            //compares date between start time and meeting date
            if(dateInRange(format.parse(postData.get(1)),format.parse(postData.get(3)), Integer.parseInt(postData.get(2)),0)){
                UserTrackingModel model=new UserTrackingModel();
                model.setTrackableId(id);
                model.setTitle(postData.get(0));
                model.setStartTime(format.parse(postData.get(1)));
                model.setEndTime(addDate(format.parse(postData.get(1)),Integer.parseInt(postData.get(2))));
                System.out.println("end time: "+model.getEndTime());
                model.setMeetingTime(format.parse(postData.get(3)));
                model.setCurrentLocation(postData.get(4));
                model.setMeetingLocation(postData.get(5));
                userTrackingList.add(model);
                userTrackingListSize = userTrackingList.size();
                return "OK";
            }
            else{
                System.out.println("Invalid date");
                return "Invalid date";
            }

            }
        catch(ParseException ex){
            Log.e(LOG_TAG,ex.toString());
            return "parse error";
        }


    }

    private int search(int id){
        int index=0;
        boolean found=false;
        for(int i=0; i<userTrackingList.size();i++){//search for tracking data
            System.out.println("size: "+userTrackingList.size());

            if(id== userTrackingList.get(i).getId()) {
                index = i;
                found=true;
                break;
            }
            else{
                found=false;
            }
        }
        if(found==false){
            return -1;
        }
        return index;
    }

    public void setData(ArrayList<UserTrackingModel>model){
        this.userTrackingList = model;
    }

    public String editData(ArrayList<String>postData, int id){
        System.out.println("id: "+postData.get(0));
        try{
            int index=0;
            index=search(Integer.valueOf(postData.get(0)));
            if(index==-1){
                System.out.println("item not found");
                return "item not found";
            }

            //compares date between start time and meeting date
            if(!format.parse(postData.get(3)).before(format.parse(postData.get(4)))){
                UserTrackingModel model=new UserTrackingModel();
                model.setId(Integer.valueOf(postData.get(0)));
                model.setTrackableId(id);
                model.setTitle(postData.get(1));
                model.setStartTime(format.parse(postData.get(2)));
                model.setEndTime((format.parse(postData.get(3))));
                System.out.println("end time: "+model.getEndTime());
                model.setMeetingTime(format.parse(postData.get(4)));
                model.setCurrentLocation(postData.get(5));
                model.setMeetingLocation(postData.get(6));
                userTrackingList.set(index,model);
                return "ok";
            }
            else{
                System.out.println("Invalid date");
                return "Invalid date";
            }


        }
        catch(ParseException ex){
            Log.e(LOG_TAG,ex.toString());
            return "parse exception";
        }


    }

    public void clearData(){
        if(this.getUserTrackingList()!=null || !this.userTrackingList.isEmpty()){
            Log.i(LOG_TAG,"user tracking list cleared");
            this.getUserTrackingList().clear();
        }

    }

    public Date addDate(Date date, int minutes){
        try{
            Calendar cal=Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MINUTE, minutes);
            String time=format.format(cal.getTime());
            System.out.println("end time: "+format.parse(time));
            //String dateString=format.format(cal.getTime());
            return format.parse(time);
        }
        catch (Exception ex){
            System.out.println("error parsing date in addDate");
            Log.e(LOG_TAG,ex.toString());
            return date;
        }

    }

    private int calculateDiff(Date start, Date end){
        return (int)(end.getTime() - start.getTime());
    }

    public void deleteData(int id){
        int index = search(id);
        if(index==-1){
            Log.i(LOG_TAG,"data not found; failed to remove data");
            return;
        }
        userTrackingList.remove(index);
        return;
    }

    public ArrayList<UserTrackingModel>getUserTrackingList(){
        return this.userTrackingList;
    }

    public int getUserTrackingListSize(){
        return this.userTrackingList.size();
    }

}
