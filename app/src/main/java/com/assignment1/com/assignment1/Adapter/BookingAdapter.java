package com.assignment1.com.assignment1.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.assignment1.com.assignment1.Controller.BookingActivityListener;
import com.assignment1.com.assignment1.Models.UserTrackingModel;
import com.assignment1.com.assignment1.R;
import com.assignment1.com.assignment1.Services.UserTrackingService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class BookingAdapter extends RecyclerView.Adapter<BookingActivityListener.ViewHolder> {
    private UserTrackingService service;
    private ArrayList<UserTrackingModel> tracking;
    private Context context;
    private static final String LOG_TAG=MainAdapter.class.getName();
    SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a", Locale.getDefault());

    public BookingAdapter(Context context){
        this.context = context;
        this.service = UserTrackingService.getSingletonInstance(context);
        this.tracking = service.getUserTrackingList();
    }
    @Override
    public BookingActivityListener.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_layout, parent, false);
        BookingActivityListener.ViewHolder vh = new BookingActivityListener.ViewHolder(v, this.context, this.tracking);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull BookingActivityListener.ViewHolder holder, int position) {
        holder.title.setText(tracking.get(position).getTitle());
        holder.time.setText(format.format(tracking.get(position).getMeetingTime()));
        holder.location.setText(tracking.get(position).getMeetingLocation());
    }

    @Override
    public int getItemCount() {
        Log.i(LOG_TAG,tracking.size()+"");
        return tracking.size();

    }
}
