package com.assignment1.com.assignment1.View;

import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.assignment1.com.assignment1.Controller.EditActivityListener;
import com.assignment1.com.assignment1.DatabaseHandler.SaveDatabase;
import com.assignment1.com.assignment1.Models.UserTrackingModel;
import com.assignment1.com.assignment1.R;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class EditActivity extends AppCompatActivity{
    private TextInputEditText nameMeeting, time;
    private Spinner spinner;
    private Button save;
    private TextView  location, truckStop;
    private int id;
    private String endDate;
    private UserTrackingModel trackingData;
    private SaveDatabase saveDb;
    private static final String LOG_TAG=EditActivity.class.getName();
    SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy KK:mm:ss a", Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        truckStop = (TextView)findViewById(R.id.txtDate);
        //stop = (TextView)findViewById(R.id.txtData);
        location = (TextView)findViewById(R.id.txtLocationData);
        nameMeeting = (TextInputEditText)findViewById(R.id.insertTitleTextInput);
        time = (TextInputEditText)findViewById(R.id.insertMeetingTimeTextInput);

        //Get data from previous activity
        trackingData=(UserTrackingModel)getIntent().getSerializableExtra("trackingData");
        location.setText(trackingData.getMeetingLocation());
        nameMeeting.setText(trackingData.getTitle());
        endDate=format.format(trackingData.getEndTime());
        time.setText(format.format(trackingData.getMeetingTime()));
        truckStop.setText(format.format(trackingData.getStartTime()));
        id=trackingData.getTrackableId();


        //Set Action bar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add a Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //SaveButton set
        save = (Button)findViewById(R.id.btnSave);
        save.setOnClickListener(new EditActivityListener.detailController(this,location,nameMeeting,time,format.format(trackingData.getStartTime()),trackingData.getId(),id, endDate));

    }

    @Override
    protected void onStop(){
        super.onStop();
        saveDb = new SaveDatabase(this);
        saveDb.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

}
