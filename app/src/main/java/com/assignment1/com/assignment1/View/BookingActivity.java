package com.assignment1.com.assignment1.View;

import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.assignment1.com.assignment1.Adapter.BookingAdapter;
import com.assignment1.com.assignment1.Controller.RecyclerViewDecorationController;
import com.assignment1.com.assignment1.DatabaseHandler.SaveDatabase;
import com.assignment1.com.assignment1.R;

public class BookingActivity extends AppCompatActivity {
    private RecyclerView myRecyclerView;
    private RecyclerView.Adapter bookingAdapter;
    private RecyclerView.LayoutManager myLayoutManager;
    private SaveDatabase saveDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        myRecyclerView = (RecyclerView) findViewById(R.id.bookingRecyclerView);
        myRecyclerView.setHasFixedSize(true);

        //set Layout for Recycler View
        myLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(myLayoutManager);

        //Set Recycler View Adapter
        bookingAdapter = new BookingAdapter(this);
        myRecyclerView.setAdapter(bookingAdapter);

        myRecyclerView.addItemDecoration(RecyclerViewDecorationController.createDecoration(this));

        //Set Action bar
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("View Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
    @Override
    protected void onStop(){
        super.onStop();
        saveDb = new SaveDatabase(this);
        saveDb.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
