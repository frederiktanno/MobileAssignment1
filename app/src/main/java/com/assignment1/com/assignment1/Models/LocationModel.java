package com.assignment1.com.assignment1.Models;

import java.io.Serializable;

public class LocationModel implements Serializable {

    private Double latitude;
    private Double longitude;

    public void setLatitude(Double latitude){
        this.latitude=latitude;
    }

    public void setLongitude(Double longitude){
        this.longitude = longitude;
    }

    public Double getLatitude(){
        return this.latitude;
    }

    public Double getLongitude(){
        return this.longitude;
    }
}
