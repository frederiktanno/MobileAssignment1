package com.assignment1.com.assignment1.Models;

import java.io.Serializable;
import java.util.Date;

public class UserTrackingModel implements Serializable{
    private int id;
    private int trackableId;
    private String title;
    private Date startTime;
    private Date endTime;
    private Date meetingTime;
    private String currentLocation;
    private String meetingLocation;

    public void setId(int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setTrackableId(int trackableId) {
        this.trackableId = trackableId;
    }

    public int getTrackableId() {
        return trackableId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getMeetingLocation() {
        return meetingLocation;
    }

    public void setMeetingLocation(String meetingLocation) {
        this.meetingLocation = meetingLocation;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setMeetingTime(Date date){
        this.meetingTime=date;
    }
    public Date getMeetingTime(){
        return this.meetingTime;
    }
}
