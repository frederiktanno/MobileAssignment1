package com.assignment1.com.assignment1.Controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.assignment1.com.assignment1.Models.UserTrackingModel;
import com.assignment1.com.assignment1.R;
import com.assignment1.com.assignment1.Services.UserTrackingService;
import com.assignment1.com.assignment1.View.BookingActivity;
import com.assignment1.com.assignment1.View.EditActivity;

import java.util.ArrayList;

public class BookingActivityListener {
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        public TextView title,time,location;
        private Context context;
        private UserTrackingService service;
        private ArrayList<UserTrackingModel> trackings;
        int id;

        public ViewHolder(View v, Context context, ArrayList<UserTrackingModel> trackings) {
            super(v);
            this.context = context;
            this.trackings = trackings;
            this.title = (TextView) v.findViewById(R.id.txtBookingTitle);
            this.time = (TextView) v.findViewById(R.id.txtMeetingTime);//cast into TextView
            this.location = (TextView) v.findViewById(R.id.txtMeetingLocation);
            this.service=UserTrackingService.getSingletonInstance(this.context);
            v.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuItem Edit = menu.add(Menu.NONE, 1, 1, "Edit");
            MenuItem Delete = menu.add(Menu.NONE, 2, 2, "Delete");
            Edit.setOnMenuItemClickListener(onMenuListener);
            Delete.setOnMenuItemClickListener(onMenuListener);
        }

        private final MenuItem.OnMenuItemClickListener onMenuListener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()){
                    //Edit
                    case 1:
                        System.out.println("adapter position: "+getAdapterPosition());
                        UserTrackingModel tracking=trackings.get(getAdapterPosition());
                        System.out.println("Id: "+tracking.getId());
                        Intent intent=new Intent(context, EditActivity.class);
                        intent.putExtra("trackingData",tracking);
                        context.startActivity(intent);
                        break;
                    //Delete
                    case 2:
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);

                        builder.setTitle("Alert")
                                .setMessage("Are you sure you want to delete this booking?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        service.deleteData(trackings.get(getAdapterPosition()).getId());
                                        Intent refresh=new Intent(context, BookingActivity.class);
                                        context.startActivity(refresh);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        break;
                }
                return true;
            }
        };
    }

}
