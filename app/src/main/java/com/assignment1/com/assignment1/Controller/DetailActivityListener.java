package com.assignment1.com.assignment1.Controller;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.Services.TrackingService;
import com.assignment1.com.assignment1.View.SaveActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailActivityListener {


    public static class detailController implements View.OnClickListener{
        private Context context;
        private ArrayList<TrackingModel> trackings;
        int id;
        String name;


        public detailController(Context context, ArrayList<TrackingModel> trackings, int id, String title){
            this.context =  context;
            this.trackings = trackings;
            this.id = id;
            this.name = name;

        }
        @Override
        public void onClick(View v) {
            Snackbar.make(v, "You pressed Fab", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            Intent intent= new Intent(this.context, SaveActivity.class);//creates a new Intent to refer to an activity
            //which will display the trackable's details
            intent.putExtra("trackableTracking",this.trackings);
            intent.putExtra("id",id);
            this.context.startActivity(intent);

        }
    }
    public static class detailMarkerController implements GoogleMap.OnMarkerClickListener {
        private Context context;
        private ArrayList<TrackingModel> trackings;
        private TrackingService service;
        private HashMap<Marker, TrackingModel> markerMeta;
        int id;
        String name;
        Marker marker;
        private String LOG_TAG = DetailActivityListener.class.getName();

        public detailMarkerController(Context context, ArrayList<TrackingModel>trackings, int id, String name, HashMap<Marker, TrackingModel>markerMeta){
            this.context = context;
            this.trackings = trackings;
            this.markerMeta = markerMeta;
            this.id = id;
            this.marker = marker;
            this.name = name;
            this.service = TrackingService.getSingletonInstance(context);
        }

        @Override
        public boolean onMarkerClick(Marker marker) {
            System.out.println(markerMeta.get(marker) + " : hi");
            if (markerMeta.get(marker) == null){
                Toast.makeText(context, "Truck doesn't stop at this point", Toast.LENGTH_SHORT).show();
                return false;
            }
            Double walk = service.getWalkingTime(markerMeta.get(marker).getIdTracking());
            Intent intent = new Intent(this.context, SaveActivity.class);
            intent.putExtra("trackableTracking",markerMeta.get(marker));
            intent.putExtra("id", id);
            intent.putExtra("walkingTime",walk);
            Log.i(LOG_TAG,"walking time:" +walk);
            this.context.startActivity(intent);
            return false;
        }
    }
}
