package com.assignment1.com.assignment1.Controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.TextView;

import com.assignment1.com.assignment1.Models.LocationModel;
import com.assignment1.com.assignment1.Services.UserTrackingService;
import com.assignment1.com.assignment1.View.BookingActivity;

import java.util.ArrayList;

public class EditActivityListener {

    public static class detailController implements View.OnClickListener{
        private Context context;
        private UserTrackingService service;
        private TextInputEditText nameMeeting, time;
        private String truckStop;
        private TextView  location;
        private ArrayList<String> data;
        private int id;
        private Double minutes;
        private String endDate;
        int idTrackable;
        private String check;
        private LocationModel loc;

        public detailController(Context context, TextView location,
                                TextInputEditText nameMeeting, TextInputEditText time, String truckStop, int id, int idTrackable, String endDate){
            this.context =  context;
            this.location = location;
            this.nameMeeting = nameMeeting;
            this.time = time;
            this.truckStop = truckStop;
            this.id=id;
            this.idTrackable=idTrackable;
            this.endDate=endDate;
            data=new ArrayList<>();
            service=UserTrackingService.getSingletonInstance(context);
            this.loc = service.getLocation();
        }

        @Override
        public void onClick(View v) {
            Snackbar.make(v, "You pressed Save Button", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            String nameMeetingData = nameMeeting.getEditableText().toString();
            data.add(id+"");
            System.out.println("id: "+id);
            data.add(nameMeetingData);
            data.add(truckStop);//get start date
            data.add(endDate);//get stopping time
            String timeData = time.getEditableText().toString();
            data.add(timeData);//meeting time
            String locationData = location.getText().toString();
            //data.add("-37.8091341, 144.9666907");//current location hardcoded
            data.add(loc.getLatitude().toString()+", "+loc.getLongitude().toString());
            data.add(locationData);
            check=service.editData(data,idTrackable);
            //sends input to be processed and persisted
            if(check.equals("parse error")){

                //service=UserTrackingService.getSingletonInstance(context);
                data.clear();
                //handle errors here
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);

                builder.setTitle("Alert")
                        .setMessage("Please Insert the Correct date Format")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
            else if(check.equals("Invalid date")){
                data.clear();
                //handle errors here
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);

                builder.setTitle("Alert")
                        .setMessage("Invalid Date value. Please insert date between the stopping time")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
            else if(check.equals("item not found")){
                data.clear();
                //handle errors here
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);

                builder.setTitle("Alert")
                        .setMessage("Item not found")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
            else{

                Intent intent= new Intent(this.context, BookingActivity.class);//creates a new Intent to refer to an activity
                this.context.startActivity(intent);
            }
        }
    }
}
