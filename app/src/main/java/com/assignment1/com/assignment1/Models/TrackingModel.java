package com.assignment1.com.assignment1.Models;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class TrackingModel implements Serializable{
    private int id;
    private int idTracking;
    private Date date;
    private int stop;
    private Double latitude;
    private Double longitude;

    public void setId(int id){
        this.id=id;
    }

    public int getIdTracking() {
        return idTracking;
    }

    public void setIdTracking(int idTracking) {
        this.idTracking = idTracking;
    }

    public int getId(){
        return this.id;
    }

    public void setDate(Date date){ this.date=date;}
    public Date getDate(){return this.date;}

    public void setStop(int stop){this.stop=stop;}
    public int getStop(){return this.stop;}

    public void setLatitude(Double latitude){this.latitude=latitude;}
    public Double getLatitude(){return this.latitude;}

    public void setLongitude(Double longitude){this.longitude=longitude;}
    public Double getLongitude(){return this.longitude;}

    @Override
    public String toString()
    {
        return String.format(Locale.getDefault(), "Date/Time=%s, trackableId=%d, stopTime=%d, lat=%.5f, long=%.5f", DateFormat.getDateTimeInstance(
                DateFormat.SHORT, DateFormat.MEDIUM).format(date), id, stop, latitude, longitude);
    }
}
