package com.assignment1.com.assignment1.Controller;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.assignment1.com.assignment1.Models.TrackableModel;
import com.assignment1.com.assignment1.Models.TrackingModel;
import com.assignment1.com.assignment1.R;
import com.assignment1.com.assignment1.Services.CardinalityService;
import com.assignment1.com.assignment1.View.BookingActivity;
import com.assignment1.com.assignment1.View.DetailActivity;

import java.util.ArrayList;


public class MainActivityListener {

    /*Listener for each foodtruck data item when they are clicked */
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView text;
        private CardinalityService service;
        private final String LOG_TAG = this.getClass().getName();
        private Context context;
        private ArrayList<TrackableModel>trackables;
        private ArrayList<TrackingModel>trackings;
        public ViewHolder(View v, Context context, ArrayList<TrackableModel>trackables) {
            super(v);
            this.context=context;
            this.trackables=trackables;
            v.setOnClickListener(this);
            this.text = (TextView) v.findViewById(R.id.txtBookingTitle); //cast into TextView

        }

        @Override
        public void onClick(View v){
            int position=getAdapterPosition();
            service=CardinalityService.getSingletonInstance(this.context);

            TrackableModel trackable=this.trackables.get(position);//gets the trackable object details
            Log.i(LOG_TAG,trackable.getName());
            trackings=service.getTrackingDataById(trackable.getId());//gets that trackable's tracking data details
            Intent intent= new Intent(this.context, DetailActivity.class);//creates a new Intent to refer to an activity
            //which will display the trackable's details
            intent.putExtra("trackableTracking",this.trackings);
            intent.putExtra("title",trackable.getName());
            intent.putExtra("trackableId",trackable.getId());
            Log.i(LOG_TAG,"Trackable id: "+trackable.getId()+"");
            this.context.startActivity(intent);
        }


    }

    public static class mainController implements View.OnClickListener{
        private Context context;

        public mainController(Context context){
            this.context = context;
        }
        @Override
        public void onClick(View v) {
            Intent intent= new Intent(this.context, BookingActivity.class);//creates a new Intent to refer to an activity
            //which will display the trackable's details
            this.context.startActivity(intent);
        }
    }
}
