package com.assignment1.com.assignment1.Services;

import android.util.Log;
import android.content.Context;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.Scanner;
import com.assignment1.com.assignment1.Models.TrackableModel;
import com.assignment1.com.assignment1.R;

/*
    This class contains functions of logical operations for trackable data
*/

public class TrackableService{

    // Singleton
    private TrackableService()
    {
    }

    private static Context context;
    ArrayList<TrackableModel> trackableData=new ArrayList<>();
    private static final String LOG_TAG=TrackableService.class.getName();

    // singleton support
    private static class LazyHolder
    {
        static final TrackableService INSTANCE = new TrackableService();
    }

    public static TrackableService getSingletonInstance(Context context){
        TrackableService.context=context;
        return LazyHolder.INSTANCE;
    }

    public void readFile(Context context){
        //this.context=context;
        trackableData.clear();
        try (Scanner scan=new Scanner(context.getResources().openRawResource(R.raw.food_truck_data))){
            scan.useDelimiter(";\\s*|\\s*\\n+");//uses the ; symbol as delimiter
            while(scan.hasNext()){
                TrackableModel model=new TrackableModel();
                model.setId(Integer.parseInt(scan.next()));
                model.setName(scan.next());
                model.setDescription(scan.next());
                model.setURL(scan.next());
                model.setCategory(scan.next());
                removeQuotes(model);
                trackableData.add(model);
            }
            scan.close();
        }
        catch(Resources.NotFoundException ex){
            Log.e(LOG_TAG, ex.toString());
        }


    }

    public ArrayList<TrackableModel>getData(){
        return this.trackableData;
    }

    public void clearData(){
        if(!this.trackableData.isEmpty() || this.trackableData!=null){
            this.trackableData.clear();
        }

    }

    public void setData(ArrayList<TrackableModel>model){
        this.trackableData = model;
    }

    public void removeQuotes(TrackableModel model){
        String name=model.getName();
        String URL = model.getURL();
        String type = model.getCategory();
        name=name.replaceAll("^\"|\"$", "");
        URL=URL.replaceAll("^\"|\"$", "");
        type=type.replaceAll("^\"|\"$", "");
        model.setCategory(type);
        model.setName(name);
        model.setURL(URL);
    }

    //return the data by category
    public ArrayList<TrackableModel> getDataByCategory(String category){
        //always re-read the file to get latest data
        readFile(context);
        ArrayList<Integer>indexList=new ArrayList<>();
        ArrayList<TrackableModel>filteredData=new ArrayList<>();

        for(int i=0; i<trackableData.size();i++){
            if(category.equals(trackableData.get(i).getCategory())){
                indexList.add(i);
            }
        }
        for(int i=0; i<indexList.size(); i++){
            filteredData.add(trackableData.get(indexList.get(i)));
        }
        return filteredData;
    }


}
